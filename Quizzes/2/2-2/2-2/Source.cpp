#include <iostream>
#include <string>

using namespace std;

int main()
{
	double num1;
	double num2;
	double num3;
	double num4;
	double num5;

	cout << "Input first number: ";
	cin >> num1;
	cout << "Input second number: ";
	cin >> num2;
	cout << "Input third number: ";
	cin >> num3;
	cout << "Input fourth number: ";
	cin >> num4;
	cout << "Input fifth number: ";
	cin >> num5;

	double ave = (num1 + num2 + num3 + num4 + num5) / 5; // Average
	
	cout << endl << "Average: " << ave << endl << endl; 

	system("pause");
	return 0;
}