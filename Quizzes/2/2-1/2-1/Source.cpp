#include <iostream>
#include <stdio.h>

using namespace std;

int main() {

	double r;

	cout << "Input radius (r) of the circle: ";
	cin >> r;

	system("cls");

	double area = 3.14 * (r * r); // area
	double cir = 2 * 3.14 * r; // circumference

	cout << "Area: " << area << endl;
	cout << "Circumference: " << cir << endl <<endl;

	system("pause");
	return 0;

}