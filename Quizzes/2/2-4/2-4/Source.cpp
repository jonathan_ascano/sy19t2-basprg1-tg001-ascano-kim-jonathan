#include <iostream>
#include <string>

using namespace std;

int main()
{
	string str = "Hello World!";
	float x = 5.0f;
	int num = 65;

	cout << str << endl;
	cout << x << endl;
	cout << "65Num = " << num << endl << endl;

	system("pause");
	return 0;
}