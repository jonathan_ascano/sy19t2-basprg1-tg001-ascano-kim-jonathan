#include <iostream>
#include <stdio.h>
#include <ctime>

using namespace std;

int main()
{
	srand(time(0));

	int minDmg;
	int maxDmg;

	cout << "Input minimum damage: ";
	cin >> minDmg;
	cout << "Input maximum damage: ";
	cin >> maxDmg;

	system("cls");

	int baseDmg = (rand() % (maxDmg - minDmg + 1) + minDmg);

	cout << "Damage dealt: " << baseDmg << endl << endl;

	system("pause");
	return 0;
}