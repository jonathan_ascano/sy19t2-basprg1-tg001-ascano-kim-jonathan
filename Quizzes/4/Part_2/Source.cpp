#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
	srand(time(0));
	int array[20];

	// A. Print random integers
	cout << "Array[20] = ";
	for (int i = 0; i < 20; i++)
	{
		array[i] = rand() % 100 + 1;
		cout << array[i] << ", ";
	}

	// using Selection Sort to find largest numbers and duplicates easier
	int p, minIndex;
	for (int i = 0; i < 20; i++)
	{
		minIndex = i;

		for (int p = i + 1; p < 20; p++)
		{
			if (array[p] < array[minIndex])
			{
				minIndex = p;
			}
		}

		if (minIndex != 1)
		{
			swap(array[i], array[minIndex]);
		}
	}

	// B. Finding the two largest numbers
	int largestNum1 = 0;
	int largestNum2 = 0;
	for (int i = 0; i < 20; i++)
	{
		if (array[i] >= largestNum1) // >= in case second largest is a duplicate
		{
			largestNum2 = largestNum1;
			largestNum1 = array[i];
		}
	}

	cout << endl << "Largest numbers: " << largestNum1 << ", " << largestNum2 << endl;

	// C. Counting duplicates
	int duplicates = 0;
	for (int i = 1; i < 20; i++)
	{
		if (array[i] == array[i - 1] || array[i] == array [i - 1] && array[i] == array[i + 1]) // "||" in case there are 3 instances of a number to be counted as 1 duplicate
		{
			duplicates++;
		}
	}
	
	cout << "Duplicates: " << duplicates << endl;

	system("pause");
	return 0;
}