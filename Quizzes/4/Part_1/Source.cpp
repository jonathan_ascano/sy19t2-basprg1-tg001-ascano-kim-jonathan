#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
	srand(time(0));

	int numbers[99];
	int missing = rand() % 100 + 1;

	// A. 99 numbers with 1 missing random number
	for (int i = 0; i < 99; i++)
	{
		numbers[i] = i + 1;

		if (numbers[i] == missing)
		{
			numbers[i] += 1;
		}

		if (numbers[i] == numbers[i - 1])
		{
			numbers[i] += 1;
		}

	}

	cout << "Numbers[99]: ";
	for (int j = 0; j < 99; j++)
	{
		cout << numbers[j] << ", ";
	}

	// B. Finding the missing number through code
	int trueSum = (99 + 1) * (99 + 2) / 2;
	int sum = 0;
	for (int i = 0; i < 99; i++)
	{
		sum += numbers[i];
	}

	int missingNum = trueSum - sum;
	cout << endl << endl << "The missing number is: " << missingNum << endl;

	system("pause");
	return 0;
}