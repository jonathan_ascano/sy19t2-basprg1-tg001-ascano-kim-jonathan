#include <iostream>
#include <string>

using namespace std;

int main()
{
	string text;

	cout << "Input text to reverse: ";
	cin >> text;

	reverse(text.begin(), text.end());
	
	cout << "Output: " << text << endl;

	system("pause");
	return 0;
}