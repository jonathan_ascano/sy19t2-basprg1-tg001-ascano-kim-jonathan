#include <iostream>
#include <string>


using namespace std;

int main()
{
	string star = "*";
	int num = -1;
	int i = 1; // number of stars per line

	while (num < 0)
	{
		cout << "Enter a number: ";
		cin >> num;
	}

	while (i <= num)
	{
	
		cout << star;
		i++;

		if (i < (num + i) - num + 1)
		{
			cout << endl;
	
		}

	}

	system("pause");
	return 0;
}