#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	srand(time(0));

	int dice1 = 1;
	int dice2 = 1;
	int sum = 1;

	while (sum % 4 != 0 || sum == 1)
	{
		dice1 = rand() % 6 + 1;
		dice2 = rand() % 6 + 1;
		sum = dice1 + dice2;

		cout << dice1 << ", " << dice2 << " ";
		system("pause");
	}

	cout << "The final rolls' sum is a multiple of 4." << endl;
	system("pause");
	return 0;
}