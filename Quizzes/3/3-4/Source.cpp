#include <iostream>

using namespace std;

int main()
{
	int sequence = -1;

	while (sequence < 0)
	{
		cout << "Enter number of fibonacci sequence: ";
		cin >> sequence;
	}

	int num1 = 1;
	int num2 = 0;
	for (int i = 0; i <= sequence; i++)
	{
		
		num2 = num1;
		cout << num2 << " ";
		num1 += i;
	}

	system("pause");
	return 0;
}