#include <iostream>
#include <time.h>

using namespace std;

int main()
{
	int numVal; // number of values
	int val; // input values
	float sum = 0;

	cout << "Input number of values to be computed: ";
	cin >> numVal;

	for (int i = 1; i <= numVal; i++)
	{
		cout << "Input value " << i << ": ";
		cin >> val;
		sum += val;
	}

	float average = sum / numVal;

	cout << endl << "Average: " << average << endl;

	system("pause");
	return 0;
}