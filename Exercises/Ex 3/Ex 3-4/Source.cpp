#include <iostream>

using namespace std;

int main()
{
	int x; // number of terms

	cout << "Input number of terms: ";
	cin >> x;

	for (int i = 1; i < x * 2; i++)
	{
		if (i % 2 != 1) // checks if it's an odd number
		{
			continue;
		}

		cout << i << " ";
	}

	cout << endl << endl;

	system("pause");
	return 0;
}