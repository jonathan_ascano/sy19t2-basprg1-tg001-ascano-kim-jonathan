#include <iostream>

using namespace std;

int main()
{
	int n = 1;
	int fac = n;

	cout << "Input number: ";
	cin >> n;

	for (int i = n - 1; i > 0; i--)
	{
		fac = fac * i; // calculates the product EXCLUDING the input number
	}

	int final = n * fac; // multiplies the input number to the product made within the loop 

	cout << n << "! = " << final << endl;

	system("pause");
	return 0;
}