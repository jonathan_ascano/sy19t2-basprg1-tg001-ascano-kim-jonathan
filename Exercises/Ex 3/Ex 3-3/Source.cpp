#include <iostream>

using namespace std;

int main()
{
	int x; // starting value
	int y; // number of terms
	int sum = 0;


	cout << "Input starting value: ";
	cin >> x;
	cout << "Input number of terms: ";
	cin >> y;

	for (int i = x; i <= (y - 1) * 1 + x; i++)
	{
		sum += i;
	}

	cout << "Sum: " << sum << endl;

	system("pause");
	return 0;
}