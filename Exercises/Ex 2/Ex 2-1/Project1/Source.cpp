#include <iostream>

using namespace std;

int main()
{
	int x;
	int y;

	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;
	cout << endl;

	bool equal = x == y;
	bool xequal = x != y;
	bool greater = x > y;
	bool less = x < y;
	bool greaterEqual = x >= y;
	bool lessEqual = x <= y;

	cout << x << " == " << y << " = " << equal << endl;
	cout << x << " != " << y << " = " << xequal << endl;
	cout << x << " > " << y << " = " << greater << endl;
	cout << x << " < " << y << " = " << less << endl;
	cout << x << " >= " << y << " = " << greaterEqual << endl;
	cout << x << " <= " << y << " = " << lessEqual << endl;

	system("pause");
	return 0;
}