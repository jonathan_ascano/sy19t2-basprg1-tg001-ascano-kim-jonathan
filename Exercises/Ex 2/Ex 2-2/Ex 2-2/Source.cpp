#include <iostream>
#include <string>
#include <Windows.h>

using namespace std;

int main()
{

	int HP;

	cout << "Input current HP: ";
	cin >> HP;

	while (HP > 100) {

		cout << "Invalid. Input current HP again: ";
		cin >> HP;
	}

	system("cls");

	if (HP == 100) {

		cout << endl << "HP is FULL." << endl;
	}

	if (HP >= 50 && HP < 100) {

		system("Color 0A");
		cout << endl << "Player is HEALTHY." << endl;
	}

	if (HP > 20 && HP <= 49) {

		system("Color 0E");
		cout << endl << "WARNING! HP is low." << endl;
	}

	if (HP >= 1 && HP <= 20) {

		system("Color 0C");
		cout << endl << "HP is CRITICAL." << endl;
	}

	if (HP == 0) {
		cout << "Player is DEAD." << endl;
	}

	system("pause");
	return 0;
}