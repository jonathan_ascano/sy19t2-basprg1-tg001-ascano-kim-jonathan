#include <iostream>
#include <string>

using namespace std;

int main()
{
	char direction;
	double x = 0;
	double y = 0;

	cout << "Current position: " << "(" << x << ", " << y << ")" << endl;
	cout << "Input movement command (w, s, a, d): ";
	cin >> direction;

	system("cls");

	switch (direction)
	{
	case 'w':
		cout << "Player moved North.\n";
		cout << "Current position: " << "(" << x << ", " << (y + 1) << ")" << endl;
		break;

	case 's':
		cout << "Player moved South.\n";
		cout << "Current position " << "(" << x << ", " << (y - 1) << ")" << endl;
		break;

	case 'a':
		cout << "Player moved West.\n";
		cout << "Current position " << "(" << (x - 1) << ", " << y << ")" << endl;
		break;

	case 'd':
		cout << "Player moved East.\n";
		cout << "Current position " << "(" << (x + 1) << ", " << y << ")" << endl;
		break;

	default:
		cout << "Invalid direction.\n";
		break;

	}
	
	system("pause");
	return 0;
}