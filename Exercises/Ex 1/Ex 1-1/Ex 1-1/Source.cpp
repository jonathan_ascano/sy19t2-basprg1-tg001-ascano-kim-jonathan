#include <iostream>
#include <string>

using namespace std;

int main()
{
	string firstName;
	string lastName;
	string ynBfast;
	int age;
	float money;
	bool bfast;

	cout << "What is your first name? ";
	cin >> firstName;

	cout << "What is your last name? ";
	cin >> lastName;

	cout << "How old are you? ";
	cin >> age;

	cout << "How much money do you have on you? ";
	cin >> money;

	cout << "Have you eaten breakfast today? (1= Yes, 0 = No) ";
	cin >> bfast;

	system("cls");

	if (bfast == true) {
		ynBfast = "Yes";
	}

	else {
		ynBfast = "No";
	}

	cout << "Name:\t" << lastName << ", " << firstName << endl << "Age:\t" << age << endl << "Money:\t" << money << endl << "Has eaten breakfast:\t" << ynBfast << endl;

	system("pause");
	return 0;
}