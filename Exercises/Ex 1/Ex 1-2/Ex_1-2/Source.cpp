#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x;
	int y;

	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;

	int sum = x + y;
	int diff = x - y;
	int prod = x * y;
	int quot = x / y;
	int mod = x % y;

	cout << x << " + " << y << " = " << sum << endl;
	cout << x << " - " << y << " = " << diff << endl;
	cout << x << " * " << y << " = " << prod << endl;
	cout << x << " / " << y << " = " << quot << endl;
	cout << x << " % " << y << " = " << mod << endl;

	system("pause");
	return 0;

}