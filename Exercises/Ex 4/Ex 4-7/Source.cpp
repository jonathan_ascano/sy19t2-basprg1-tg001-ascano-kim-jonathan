#include <iostream>
#include <string>

using namespace std;

int main()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };
	
	int largestNum = INT_MIN;
	for (int i = 0; i < 10; i++)
	{
		if (numbers[i] > largestNum)
		{
			largestNum = numbers[i];
		}
	}

	cout << "The largest number is " << largestNum << endl;

	system("pause");
	return 0;
}