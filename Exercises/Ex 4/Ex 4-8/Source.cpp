#include <iostream>
#include <string>

using namespace std;

int main()
{
	cout << "Enter ten unsorted integers..." << endl;
	
	// User input
	int array[10];
	for (int i = 0; i < 10; i++)
	{
		cout << "[" << i << "]" << " = ";
		cin >> array[i];
	}
	
	// Input list
	cout << "Unsorted list = ";
	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << ", ";
	}

	cout << endl << "Sorting..." << endl;
	
	// Sorting
	int p, minIndex;
	for (int i = 0; i < (10 - 1); i++)
	{
		minIndex = i;

		for (int p = i + 1; p < 10; p++)
		{
			if (array[p] < array[minIndex])
			{
				minIndex = p;
			}
		}

		if (minIndex != i)
		{
			swap(array[i], array[minIndex]);
		}
	}

	// Sorted list
	cout << "Sorted List: ";
	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << ", ";
	}
	
	cout << endl; 
	system("pause");
	return 0;
}