#include <iostream>
#include <string>

using namespace std;

int main()
{
	int numbers1[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	int numberSum[5];

	for (int i = 0; i < 5; i++)
	{
		numberSum[i] = numbers1[i] + numbers2[i];
		cout << numberSum[i] << endl;
	}

	system("pause");
	return 0;
}