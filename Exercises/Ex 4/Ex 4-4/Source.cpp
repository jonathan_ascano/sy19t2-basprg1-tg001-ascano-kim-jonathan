#include <iostream>
#include <string>

using namespace std;

int main()
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int ArraySize = 8;

	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	int index = -1;
	for (int i = 0; i < ArraySize; i++)
	{
		if (itemToFind == items[i])
		{
			index = i;
			break;
		}
	}

	if (index >= 0)
	{
		cout << itemToFind << " found at index " << index << endl;
	}

	else
	{
		cout << itemToFind << " does not exist." << endl;
	}

	system("pause");
	return 0;
}