#include <iostream>
#include <string>

using namespace std;

int main()
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int ArraySize = 8;
	
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	bool itemExists = false;
	for (int i = 0; i < ArraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemExists = true;
			break;
		}
	}

	if (itemExists)
	{
		cout << itemToFind << " exists." << endl;
 	}

	else
	{
		cout << itemToFind << " doesn't exist." << endl;
	}

	system("pause");
	return 0;
}