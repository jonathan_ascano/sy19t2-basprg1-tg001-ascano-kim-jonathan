#include <iostream>
#include <string>

using namespace std;

int main()
{
	cout << "Enter ten unsorted integers..." << endl;

	// User input
	int array[10];
	for (int i = 0; i < 10; i++)
	{
		cout << "[" << i << "]" << " = ";
		cin >> array[i];
	}

	// Input list
	cout << "Unsorted list = ";
	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << ", ";
	}

	cout << endl << "Sorting..." << endl;

	// Sorting
	for (int i = 0; i < 10; i++)
	{
		int swaps = 0;

		for (int p = 0; p < 10 - i - 1; p++)
		{
			if (array[p] > array[p + 1])
			{
				swap(array[p], array[p + 1]);
				swaps = 1;
			}
		}
		if (!swaps)
			break;
	}

	// Sorted list
	cout << "Sorted List: ";
	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << ", ";
	}

	cout << endl;

	system("pause");
	return 0;
}