#include <iostream>
#include <string>

using namespace std;

int main()
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int ArraySize = 8;
	
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	int itemCount = 0;
	for (int i = 0; i < ArraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemCount++;
		}
	}
	
	cout << "You have " << itemCount << " " << itemToFind << endl;

	system("pause");
	return 0;
}