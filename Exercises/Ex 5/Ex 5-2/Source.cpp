#include <iostream>
#include <string>

using namespace std;

const char* getHpStatus(int hp) // doesn't run
{
	enum hpStatus { Full = 100, Healthy = 50, Low = 20, Critical = 1, Dead = 0 };

	enum hpStatus s = hp;
	switch (s)
	{
	case Full: return "HP is Full.";
	case Healthy: return "Player is Healthy.";
	case Low: return "HP is Low.";
	case Critical: return "HP is Critical!";
	case Dead: return "Player is Dead.";
	default: return "Invalid input.";
	}
}
int main()
{
	int hp;
	cout << "Input current HP: ";
	cin >> hp;

	const char* result = getHpStatus(hp);

	system("pause");
	return 0;
}