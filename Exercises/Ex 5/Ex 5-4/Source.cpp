#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

// Fill array with random numbers
void fillRandomArray(int array[20])
{
	srand(time(0));
	for (int i = 0; i < 20; i++)
	{
		array[i] = rand() % 100 + 1;
	}
}

int main()
{
	int array[20];
	fillRandomArray(array);

	cout << "Array: ";
	for (int i = 0; i < 20; i++)
	{
		cout << array[i] << ", ";
	}

	cout << endl;
	system("pause");
	return 0;
}