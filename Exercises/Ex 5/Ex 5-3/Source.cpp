#include <iostream>
#include <string>
#include <time.h>

using namespace std;

// 4-2
void addIntArrays()
{
	int array1[] = { 1, 2, 3, 4, 5 };
	int array2[] = { 0, 7, -10, 5, 3 };
	int arraySum[5];
	for (int i = 0; i < 5; i++)
	{
		arraySum[i] = array1[i] + array2[i];
		cout << arraySum[i] << endl;
	}
}

// 4-3
string findItem(string result)
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;
	bool itemExists = false;

	for (int i = 0; i < arraySize; i++)
	{
		if (result == items[i])
		{
			itemExists = true;
			break;
		}
	}

	if (itemExists)
	{
		result = " exists.";
	}

	else
	{
		result = " doesn't exist.";
	}

	return result;
}

// 4-4
int linearSearch(string item)
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;
	int index = -1;
	for (int i = 0; i < 8; i++)
	{
		if (item == items[i])
		{
			index = i;
			break;
		}
	}

	if (index >= 0)
	{
		return index;
	}

	else
	{
		cout << "--item does not exist--" << endl;
	}
}

// 4-5
int countInstances(string item)
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;
	int count = 0;
	for (int i = 0; i < 8; i++)
	{
		if (item == items[i])
		{
			count++;
		}
	}

	return count;
}

// 4-6
string getRandomElement(string random)
{
	string items[] = { "RedPotion", "BluePotion", "YgdrassilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	srand(time(0));
	while (true)
	{
		int index = rand() % arraySize;

		cout << items[index] << endl;
		random = items[index];
		system("pause");
	}

	return random;
}

// 4-7
void getLargestNum()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };
	int largest = INT_MIN;
	for (int i = 0; i < 10; i++)
	{
		if (numbers[i] > largest)
		{
			largest = numbers[i];
		}
	}
	
	cout << "The largest number is: " << largest << endl;
}

// 4-8
void selectionSort(int array[10])
{
	int p, minIndex;
	for (int i = 0; i < (10 - 1); i++)
	{
		minIndex = i;

		for (int p = i + 1; p < 10; p++)
		{
			if (array[p] < array[minIndex])
			{
				minIndex = p;
			}
		}

		if (minIndex != i)
		{
			swap(array[i], array[minIndex]);
		}
	}
}

// 4-9
void bubbleSort(int array[10])
{
	for (int i = 0; i < 10; i++)
	{
		int swaps = 0;
		for (int p = 0; p < 10 - i - 1; p++)
		{
			if (array[p] > array[p + 1])
			{
				swap(array[p], array[p + 1]);
				swaps = 1;
			}
		}

		if (!swaps)
			break;
	}
}

// for 4-8 and 4-9
void printArray(int array[10])
{
	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << ", ";
	}
}


int main()
{
	system("pause");
	return 0;
}