#include <iostream>
#include <string>

using namespace std;

int factorial(int n)
{
	int fac = n;

	if (n == 0)
	{
		fac = 1;
	}

	for (int i = 1; i < n; i++)
	{
		fac *= i;
	}

	return fac;
}

int main()
{
	int num;
	cout << "Input number: ";
	cin >> num;

	int result = factorial(num);

	cout << num << "! = " << result << endl;

	system("pause");
	return 0;
}