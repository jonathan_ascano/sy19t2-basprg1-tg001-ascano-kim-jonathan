#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	string heroName;
	int heroHP;
	int dwightHP;
	int hminDmg; // Hero min damage
	int hmaxDmg;
	int dminDmg; // Dwight min damage
	int dmaxDmg;
	// Hero
	cout << "Initializing Hero..." << endl << endl;
	cout << "Input Hero name: ";
	cin >> heroName;
	cout << "=============================================" << endl;
	cout << "Input HP for " << heroName << ": ";
	cin >> heroHP;
	cout << "Input " << heroName << "'s minimum damage: ";
	cin >> hminDmg;
	cout << "Input " << heroName << "'s maximum damage. (Value must be greater than or equal to minimum damage): ";
	cin >> hmaxDmg;

	system("cls");
	// Dwight
	cout << "Initializing Dwight, The Beet King..." << endl << endl;
	cout << "\t'Bears. Beets. Battlestar Galactica.'" << endl;
	cout << "=============================================" << endl;
	cout << "Input HP for Dwight: ";
	cin >> dwightHP;
	cout << "Input Dwight's minimum damage: ";
	cin >> dminDmg;
	cout << "Input Dwight's maximum damage. (Value must be greater than or equal to minimum damage): ";
	cin >> dmaxDmg;

	system("cls");

	while (heroHP > 0 && dwightHP > 0) {

		srand(time(0));

		char heroMove;
		int dwightMove = (rand() % 3) + 1; // 1 = attack, 2 = defend, 3 = wild attack
		int heroDmg{};
		int dwightDmg;

		cout << heroName << "'s HP: " << heroHP << endl;
		cout << "Dwight's HP: " << dwightHP << endl << endl;
		cout << "Input your action: " << endl;
		cout << "==================" << endl;
		cout << "\t" << "[a] = Attack" << endl;
		cout << "\t" << "[d] = Defend" << endl;
		cout << "\t" << "[w] = Wild Attack" << endl;
		cin >> heroMove;

		system("cls");

		if (heroMove == 'a' && dwightMove == 1) { // Hero attacks, Dwight atttacks

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg);
			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg);

			cout << heroName << " attacks..." << endl;
			cout << "Dwight attacks..." << endl << endl;
			cout << heroName << " dealt " << heroDmg << " damage!" << endl;
			cout << "Dwight dealt " << dwightDmg << " damage!" << endl << endl;
			cout << "===========================" << endl;
			cout << "Dwight: A worthy adversary!" << endl;
			cout << "===========================" << endl << endl;

			heroHP = heroHP - dwightDmg;
			dwightHP = dwightHP - heroDmg;
		}
		
		if (heroMove == 'a' && dwightMove == 2) { // Hero attacks, Dwight defends

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg) / 2;

			cout << heroName <<" attacks..." << endl;
			cout << "Dwight defends..." << endl << endl;
			cout << heroName << " dealt " << heroDmg << " reduced damage!" << endl << endl;
			cout << "================================================================" << endl;
			cout << "Dwight: Predictable. Well, HALF predictable... and half painful." << endl;
			cout << "================================================================" << endl << endl;

			dwightHP = dwightHP - heroDmg;
		}
		       
		if (heroMove == 'a' && dwightMove == 3) { // Hero attacks, Dwight wild attacks

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg);
			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg) * 2;

			cout << heroName << " attacks..." << endl;
			cout << "Dwight prepares for a wild attack..." << endl << endl;
			cout << heroName << " dealt " << heroDmg << " damage!" << endl;
			cout << "Dwight dealt " << dwightDmg << " damage. CRITICAL HIT!" << endl << endl;
			cout << "=======================" << endl;
			cout << "Dwight: I am the Alpha!" << endl;
			cout << "=======================" << endl << endl;

			heroHP = heroHP - dwightDmg;
			dwightHP = dwightHP - heroDmg;
		}

		if (heroMove == 'd' && dwightMove == 1) { // Hero defends, Dwight attacks

			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg) / 2;

			cout << heroName << " defends..." << endl;
			cout << "Dwight attacks..." << endl << endl;
			cout << "Dwight dealt " << dwightDmg << " reduced damage!" << endl << endl;
			cout << "======================================================" << endl;
			cout << "Dwight: Stop cowering behind your shield and fight me!" << endl;
			cout << "======================================================" << endl << endl;

			heroHP = heroHP - dwightDmg;
		}

		if (heroMove == 'd' && dwightMove == 2) { // Hero defends, Dwight defends

			cout << heroName << " defends..." << endl;
			cout << "Dwight defends..." << endl << endl;
			cout << "Nothing happened." << endl << endl;
			cout << "=================================================================" << endl;
			cout << "Dwight: Are we gonna stand here all day or are we going to fight?" << endl;
			cout << "=================================================================" << endl << endl;
		}

		if (heroMove == 'd' && dwightMove == 3) { // Hero defends, Dwight wild attacks

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg) * 2;

			cout << heroName << " defends..." << endl;
			cout << "Dwight prepares for a wild atack..." << endl << endl;
			cout << "Dwight missed." << endl;
			cout << heroName << " countered and dealt " << heroDmg << " damage. CRITICAL HIT!" << endl << endl;
			cout << "==============================================================" << endl;
			cout << "Dwight: ARGH. You took me from behind... That's what she said." << endl;
			cout << "==============================================================" << endl << endl;

			dwightHP = dwightHP - heroDmg;
		}

		if (heroMove == 'w' && dwightMove == 1) { // Hero wild attacks, Dwight attacks

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg) * 2;
			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg);

			cout << heroName << " prepares for a wild attack..." << endl;
			cout << "Dwight attacks..." << endl << endl;
			cout << heroName << " dealt " << heroDmg << " damage. CRITICAL HIT!" << endl;
			cout << "Dwight dealt " << dwightDmg << " damage!" << endl << endl;
			cout << "=======================================================" << endl;
			cout << "Dwight: OW! Okay. You're taking this WAY too seriously." << endl;
			cout << "=======================================================" << endl << endl;

			heroHP = heroHP - dwightDmg;
			dwightHP = dwightHP - heroDmg;
		}

		if (heroMove == 'w' && dwightMove == 2) { // Hero wild attacks, Dwight defends
			
			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg) * 2;

			cout << heroName << " prepares for a wild attack..." << endl;
			cout << "Dwight defends..." << endl << endl;
			cout << heroName << " missed!" << endl;
			cout << "Dwight took " << heroName << " from behind and dealt " << dwightDmg << " damage. CRITICAL HIT!" << endl << endl;
			cout << "===========================================" << endl;
			cout << "Dwight: I am faster than 80% of all snakes." << endl;
			cout << "===========================================" << endl << endl;

			heroHP = heroHP - dwightDmg;
		}

		if (heroMove == 'w' && dwightMove == 3) { // Hero wild attacks, Dwight wild attacks

			heroDmg = (rand() % (hmaxDmg - hminDmg + 1) + hminDmg) * 2;
			dwightDmg = (rand() % (dmaxDmg - dminDmg + 1) + dminDmg) * 2;

			cout << heroName << " prepares for a wild attack..." << endl;
			cout << "Dwight prepares for a wild attack..." << endl << endl;
			cout << "===========================================================================" << endl;
			cout << "Dwight: Don't hold back on me now. Show me what you can do with your sword!" << endl;
			cout << heroName << ": That's what she... *AHEM*" << endl;
			cout << "===========================================================================" << endl << endl;
			cout << heroName << " dealt " << heroDmg << " damage. CRITICAL HIT!" << endl;
			cout << "Dwight dealt " << dwightDmg << " damage. CRITICAL HIT!" << endl << endl << endl;

			heroHP = heroHP - dwightDmg;
			dwightHP = dwightHP - heroDmg;
		}

		system("pause");
		system("cls");
	}
	// Winner selection
	if (heroHP <= 0 && dwightHP > 0) { // Dwight wins

		cout << "Dwight struck the fatal blow." << endl;
		cout << "=============================" << endl;
		system("pause");
		cout << endl << heroName << ": You beat me." << endl;
		system("pause");
		cout << endl << heroName << ": You are the superior being." << endl;
		system("pause");
		cout << endl << "Dwight: In battle, there is no such thing as mercy." << endl;
		system("pause");
		cout << endl << "Dwight: In battle, mercy is 'ouch, I hurt my leg, I can't run, I get stabbed in the eye, and I'm dead." << endl;
		system("pause");
		cout << endl << "Dwight: Well, I'm NOT dead. I stab you in the eye." << endl;
		system("pause");
		cout << endl << "Dwight: You're dead." << endl;
	}

	if (dwightHP <= 0 && heroHP > 0) { // Hero wins
		
		cout << heroName << " struck the fatal blow." << endl;
		cout << "=============================" << endl;
		system("pause");
		cout << endl << "Dwight: Are you trying to kill me and hurt my feelings? Because if so you are succeeding." << endl;
		system("pause");
		cout << endl << "Dwight: Fortunately my feelings regenerate at twice the speed of a normal man's." << endl;
		system("pause");
		cout << endl << "Dwight: My health, however, ..." << endl;
		system("pause");
		cout << endl << "The Beet King took his final breath and succumbed to his injuries." << endl;
		system("pause");
		cout << endl << "The hero, " << heroName << ", took the King's beet-stained crown as a trophy and emerged victorious." << endl;
	}

	if (heroHP <= 0 && dwightHP <= 0) { // Draw

		cout << "The battle resulted in a draw." << endl;
		cout << "==============================" << endl;
		system("pause");
		cout << endl << "Dwight: I demand a rematch!" << endl;
	}

	system("pause");
	return 0;
}